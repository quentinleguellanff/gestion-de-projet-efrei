# Naître Enchantés Integration

## Last Changelog

### 1.0.0 Release (9/12/2021)
- Milestone : <https://framagit.org/quentinleguellanff/gestion-de-projet-efrei/-/milestones/2>
- Fix #1 HTML basefile
- Fix #2 Navbar
- Fix #3 page body content
- Fix #4 Footer
- Fix #6 fontAwesome icons

## Preview

![A simple screenshot](./assets/screenshot.png "Naître Echantés preview")

## Project setup

Just clone this project and run index.html in your favorite web browser