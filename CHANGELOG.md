# 1.0.0 Release (9/12/2021)
- Milestone : <https://framagit.org/quentinleguellanff/gestion-de-projet-efrei/-/milestones/2>
- Fix #1 HTML basefile
- Fix #2 Navbar
- Fix #3 page body content
- Fix #4 Footer
- Fix #5 fontAwesome icons